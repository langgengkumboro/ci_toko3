<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Jabatan_models extends CI_Model
{ 
	//panggil nama tabel
	private $_table = "jabatan";

	public function tampilDataJabatan()
		{
			//seperti : select * from <nama_table>
			return $this->db->get($this->_table)->result();
		}

	public function tampilDataJabatan2()
		{
			//KETIKA MAKE QUERY
			$query = $this->db->query("SELECT * FROM jabatan WHERE flag = 1");
			return $query->result();
		}

	public function tampilDataJabatan3()
		{
			//MAKE QUERY BUILDER
			$this->db->select('*');
			$this->db->order_by('kode_jabatan', 'ASC');
			$result = $this->db->get($this->_table);
			return $result->result();
		}
		public function save()
		{
			$data['kode_jabatan'] =$this->input->post('kode_jabatan');
			$data['nama_jabatan'] =$this->input->post('nama_jabatan');
			$data['keterangan'] =$this->input->post('keterangan');
			$data['flag'] =1;
			$this->db->insert($this->_table, $data);
			//catetan $data['nama_lengkap']<-(seseuai database) =$this->input->post('nama_karyawan')<-seseuai inputkaryawan;
		}
	public function detail($kode_jabatan)
		{
			$this->db->select('*');
			$this->db->where('kode_jabatan', $kode_jabatan);
			$this->db->where('flag', 1);
			$result = $this->db->get($this->_table);
			return $result->result();
		}

	public function update($kode_jabatan)
		{
			
			$data['nama_jabatan'] =$this->input->post('nama_jabatan');
			$data['keterangan'] =$this->input->post('keterangan');
			$data['flag'] =1;
			

			$this->db->where('kode_jabatan', $kode_jabatan);
			$this->db->update($this->_table, $data);
		
		}

		public function delete($kode_jabatan)
	
	{
		$this->db->where('kode_jabatan',$kode_jabatan);
		$this->db->delete($this->_table);
	}





} 